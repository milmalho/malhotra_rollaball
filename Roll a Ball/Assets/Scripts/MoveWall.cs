﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveWall : MonoBehaviour
{
    //Create the public function to translate the wall down
    public static void moveAWall(GameObject wallToDestroy)
    {
        wallToDestroy.transform.Translate(new Vector3(0, -1.63f, 0) * Time.deltaTime);
    } 
}
