﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public Text countText;
    public Text winText;

    private Rigidbody rb;
    private int count;

    public GameObject wallToDestroy;

    //Checks layers to look out for that are ground layers
    public LayerMask groundLayers;
    //How high I want to jump
    public float jumpForce = 7;
    //Collider
    public SphereCollider col;

    public PanCam other;

    private int level;

    private bool panCamEnabled = false;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        level = 1;
        SetCountText();
        winText.text = "";
        //Grabs collider reference
        col = GetComponent<SphereCollider>();
        

    }

    void FixedUpdate()
    {
        //Lets player move on x and z axis
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        //If pancam is enabled or when all items are collected, stop all motion and input of player
        if (panCamEnabled || count == 15)
        {
            rb.AddForce(new Vector3(0, 0, 0));
            rb.angularVelocity = Vector3.zero;
        }
        else //let player move
        {
            rb.AddForce(movement * speed);
        }
       
    }
    private void Update()
    {
        //If player presses space bar
        if (IsGrounded() && Input.GetKeyDown(KeyCode.Space))
        {
            //Gives player ability to jump
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        }
        //Once 12 cubes are picked up and on level 1, move the wall down
        if (count == 12 && level == 1)
        {
            MoveWall.moveAWall(wallToDestroy);
            //Starts the coroutine for switching the cameras
            StartCoroutine(Panera());
            
        }
       
    }
    //Pans the camera and updates the levels 
    IEnumerator Panera()
    {
        other.ShowPanCam();
        panCamEnabled = true;
        yield return new WaitForSeconds(3);
        
        other.ShowPlayerCam();
        panCamEnabled = false;
        level = 2;
    }
   
    //Checks if the ball is at the specified distance away from the ground and prevents another jump (prevents infinite jump)
    private bool IsGrounded()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x,
            col.bounds.min.y, col.bounds.center.z), col.radius * .9f, groundLayers);
    }

    //Allows the objects to be picked up and increases the count by 1
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Pick up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }
    }
    //Once all the items are picked up (15 items) display win text
    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 15)
        {
            winText.text = "You Win!";
        }
    }
}