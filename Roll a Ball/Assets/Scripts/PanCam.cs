﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanCam : MonoBehaviour
{
    public Camera playerCam;
    public Camera panCam;

    //Function that enables tha player camera
    public void ShowPlayerCam()
    {
        playerCam.enabled = true;
        panCam.enabled = false;
    }
    //Function that enables the pan camera
    public void ShowPanCam()
    {
        playerCam.enabled = false;
        panCam.enabled = true;
    }
}
